<?php

namespace App\Traits;

use ReflectionClass;
use App\Observers\ModelHashIdObserver;

trait UsesHashIds
{
    public static function bootUsesHashIds()
    {
        static::observe(new ModelHashIdObserver());

        static::creating(function ($model) {
            $model->fillable(array_merge($model->fillable, ['hash_id']));
        });
    }

    public function getRouteKeyName()
    {
        return 'hash_id';
    }

    public function getId()
    {
        return $this->hash_id;
    }

    public function getHashShortName()
    {
        return strtolower(str_limit((new ReflectionClass($this))->getShortName(), 4, ''));
    }
}
