<?php

namespace App;

use App\Traits\UsesHashIds;
use App\Observers\ModelHashIdObserver;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use UsesHashIds;

    public function items()
    {
        return $this->hasMany(Item::class, 'invoice_id', 'hash_id');
    }
}
