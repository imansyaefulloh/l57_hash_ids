<?php

namespace App;

use App\Traits\UsesHashIds;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use UsesHashIds;

    protected $fillable = [
        'invoice_id'
    ];
}
